import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';


export default class App3 extends Component{

    render(){
        return(

            <View style={styles.mainBack}>
                <View style={styles.row}>
                    <Text style={styles.textIcon}>icon</Text>
                    <Text style={styles.textIconMidle}>icon</Text>
                    <Text style={styles.textIcon}>icon</Text>

                </View>
                <View style={styles.column}>
                    <Text>scrollView</Text>
                </View>
            
            
            </View>

        )
    }



}


const styles = StyleSheet.create({

    mainBack:{
        backgroundColor:'white',
        flex:1
    },

    row:{
        flexDirection:'row',
       
    },

    textIcon:{
        backgroundColor:'green',
        margin:5,
        flex:1,
        margin:2,
        height:40
    },

    textIconMidle:{
        backgroundColor:'green',
        margin:5,
        flex:2,
        margin:2,
        height:40
    },
    column:{
        flex:1,
        backgroundColor:'blue',
        zIndex:2,
        margin:5
    }

})