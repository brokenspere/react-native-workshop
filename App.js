/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerText}>News</Text>
        </View>

        <View style={styles.content}>
        
         <View style={styles.row}>
            <View style={styles.box1}>
                <Text>Lorem</Text>
            </View>
            <View style={styles.box2}>
                <Text>Lorem</Text>
            </View>
          
          </View>

          <View style={styles.row}>
            <View style={styles.box1}>
                <Text>Lorem</Text>
            </View>
            <View style={styles.box2}>
                <Text>Lorem</Text>
            </View>
          
          </View>
         
          <View style={styles.row}>
            <View style={styles.box1}>
                <Text>Lorem</Text>
            </View>
            <View style={styles.box2}>
                <Text>Lorem</Text>
            </View>
          
          </View>

        
      
        
         </View>
    
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'blue',
  },
  header:{
    alignItems:'center',
    backgroundColor:'red'
  },
  headerText:{
    color:'white',
    fontSize:20,
    fontWeight:'bold',
    padding:30
  },
  content:{
    backgroundColor:'yellow',
    flex:1,
    flexDirection:'row',
   
  },
  box1:{
    backgroundColor:'green',
    flex:1,
    margin:14
  },
  box2:{
    backgroundColor:'purple',
    flex:1,
    margin:14
  },
  row:{
    backgroundColor:'cyan',
    flex:1,
    flexDirection:'row'    
  }
});
